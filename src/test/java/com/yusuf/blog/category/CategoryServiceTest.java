package com.yusuf.blog.category;

import com.yusuf.blog.category.Category;
import com.yusuf.blog.category.CategoryService;
import com.yusuf.blog.config.TestConfiguration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by yusuf on 25.09.2017.
 */


public class CategoryServiceTest extends TestConfiguration {


    @Autowired
    CategoryService categoryService;
    Category category = new Category();


    @Before
    public void setUp() {


        category.setCategoryName("blog projem");
        category.setCategoryDescription("spring mvc test ");
        categoryService.save(category);

    }


    @Test
    public void saveTest() {

        List<Category> beforeList = categoryService.findAll();

        category = new Category();
        category.setCategoryDescription("test description");
        category.setCategoryName("test java");
        categoryService.save(category);


        List<Category> afterList = categoryService.findAll();

        assert (afterList.size() > beforeList.size());


    }

    @Test
    public void deleteTest() {


        List<Category> beforeList = categoryService.findAll();

        categoryService.delete(category);

        List<Category> afterList = categoryService.findAll();

        Assert.assertNotEquals(beforeList.size(), afterList.size());


    }

    @Test
    public void findOneTest() {


        assert (null != categoryService.findOne(category.getId()));


    }


}
