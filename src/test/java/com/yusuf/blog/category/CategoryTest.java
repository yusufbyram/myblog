package com.yusuf.blog.category;

import com.yusuf.blog.config.TestConfiguration;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

/**
 * Created by yusuf on 30.09.2017.
 */
public class CategoryTest extends TestConfiguration {


    private static Validator validator;

    private static Set<ConstraintViolation<Category>> constraintViolations;

    Category category = new Category();

    @BeforeClass
    public static void setUpValidator() {

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Before
    public void setUp() {


        category.setCategoryName("blog projem");
        category.setCategoryDescription("spring mvc test ");


    }


    @Test
    public void CategoryNameNotNullValid() {


        category.setCategoryName(null);
        constraintViolations = validator.validate(category);
        Assert.assertEquals(1, constraintViolations.size());

    }


}
