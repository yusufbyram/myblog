package com.yusuf.blog.config;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by yusuf on 25.09.2017.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(  classes = {SpringWebTestConfig.class,  SpringJpaTestConfiguration.class})
@Transactional

public abstract class TestConfiguration {



    @BeforeClass
    public static void beforeAllTests() {
        System.out.println("configure implementleri calistiktan hemen sonra burası calisir.");

    }
    @AfterClass
    public static void afterAllTests(){
        System.out.println("tum testler bittikten sonra calisir.");
    }

}
