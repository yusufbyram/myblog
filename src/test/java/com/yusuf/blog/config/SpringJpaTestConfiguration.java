package com.yusuf.blog.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@PropertySource("classpath:jdbc-h2.properties")
@EnableJpaRepositories(basePackages = "com.yusuf.blog")
@EnableTransactionManagement
public class SpringJpaTestConfiguration {

    @Value("${db.url}")
    private String dbUrl;

    @Value("${db.userName}")
    private String userName;

    @Value("${db.driver}")
    private String dbDriver;

    @Value("${db.passWord}")
    private String passWord;

    @Bean
    public EntityManagerFactory entityManagerFactory() {

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        vendorAdapter.setGenerateDdl(true);

        Properties propertiesJpa = new Properties();
        propertiesJpa.put("hibernate.hbm2ddl.auto", "create-drop");


        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);

        factory.setPackagesToScan("com.yusuf.blog");
        factory.setJpaProperties(propertiesJpa);
        factory.setDataSource(dataSource());
        factory.afterPropertiesSet();

        return factory.getObject();
    }

    @Bean
    public PlatformTransactionManager transactionManager() {

        JpaTransactionManager txManager = new JpaTransactionManager();
        txManager.setEntityManagerFactory(entityManagerFactory());
        return txManager;
    }


    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(dbDriver);
        dataSource.setUrl(dbUrl);
        dataSource.setUsername(userName);
        dataSource.setPassword(passWord);



			/*	 dataSource.setDriverClassName("org.postgresql.Driver");
         dataSource.setUrl("babar.elephantsql.com");
		 dataSource.setUsername( "senypyhl" );
		 dataSource.setPassword( "H6lXEVUw5Iq29V3LnH6DI59JCiwbmYjq" );
*/

        return dataSource;

    }

}