<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="code"  uri="/WEB-INF/Custom-Tld/CodeView.tld"   %>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>

<!DOCTYPE html>
<html lang="tr">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Blog Home - Start Bootstrap Template</title>
	<link rel="stylesheet" href="${pageContent.request.contextPath}/blog/resources/templates/ace-master/assets/css/prettify.min.css" />
	<link rel="stylesheet" href="${pageContent.request.contextPath}/blog/resources/templates/ace-master/assets/css/readMore.css" />

	<!-- Bootstrap Core CSS -->
	<link href="${pageContent.request.contextPath}/blog/resources/templates/startbootstrap-blog-home-gh-pages/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom CSS -->
	<link href="${pageContent.request.contextPath}/blog/resources/templates/startbootstrap-blog-home-gh-pages/css/blog-home.css" rel="stylesheet">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

	<decorator:head/>


</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">Start Bootstrap</a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li>

					<a href="#">Hakkımda</a>
				</li>
				<li>


					<a href="#">Services</a>
				</li>
				<li>
					<a href="#">Contact</a>
				</li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container">

	<div class="row">

		<!-- Blog Entries Column -->
		<div class="col-md-8">



			<decorator:body/>




			<p class="lead">

			</p>


			<hr>








		</div>

		<!-- Blog Sidebar Widgets Column -->
		<div class="col-md-4">

			<!-- Blog Search Well -->
			<div class="well">
				<h4>Blog Search</h4>
				<div class="input-group">
					<input type="text" class="form-control">
					<span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <span class="glyphicon glyphicon-search"></span>
                        </button>
                        </span>
				</div>
				<!-- /.input-group -->
			</div>

			<!-- Blog Categories Well -->
			<div class="well">
				<h4>Blog Categories</h4>




				<div class="row">
					<div class="col-lg-12">
						<ul class="list-unstyled">

							<c:forEach var="item" items="${categoryList}">
								<li><a id="deneme" href="${pageContext.request.contextPath}/home/category?categoryId=${item.id}">${item.categoryName}</a>
								</li>
							</c:forEach>

						</ul>
					</div>
					<!-- /.col-lg-6 -->

					<!-- /.col-lg-6 -->
				</div>
				<!-- /.row -->
			</div>

			<!-- Side Widget Well -->
			<div class="well">
				<h4>Side Widget Well</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
			</div>

		</div>

	</div>
	<!-- /.row -->

	<hr>

	<!-- Footer -->
	<footer>
		<div class="row">
			<div class="col-lg-12">
				<p>Copyright &copy; Yusuf Bayram 2017</p>
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<!-- /.row -->
	</footer>

</div>
<!-- /.container -->




</div>
<script src="http://code.jquery.com/jquery-1.12.1.min.js"></script>

<script type="text/javascript">

	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-36251023-1']);
	_gaq.push(['_setDomainName', 'jqueryscript.net']);
	_gaq.push(['_trackPageview']);

	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();

</script>
<script type="text/javascript">




	$(function () {
		$("#deneme").click  (function () {


			alert('dsfsdf');

			var postUrl = "../cevretemizlik/faaliyet-nevi-aciklama.json";
			var json = {faaliyetKod: $("#ftSel").val()};
			$.ajax({
				url: postUrl,
				type: "GET",
				data: json,
				dataType: "json",
				cache: true,
				beforeSend: function (xhr) {
					xhr.setRequestHeader("Accept", "application/json");
					xhr.setRequestHeader("Content-Type", "application/json");
				},
				success: function (data) {

					for (var i in data.items) {
						var el = data.items[i];

						$("#kapasiteNviAciklama").text(el.text + "");
					}
				},
				error: function () {
					alert("error");
				}


			});

		});
	});

</script>

<script src="${pageContent.request.contextPath}/blog/resources/templates/ace-master/assets/js/readMore.js"></script>

<script src="${pageContent.request.contextPath}/blog/resources/templates/ace-master/assets/js/jquery-2.1.4.min.js"></script>
<script src="${pageContent.request.contextPath}/blog/resources/templates/ace-master/assets/js/jquery-1.11.3.min.js"></script>
<script src="${pageContent.request.contextPath}/blog/resources/templates/ace-master/assets/js/ace-elements.min.js"></script>
<script src="${pageContent.request.contextPath}/blog/resources/templates/ace-master/assets/js/ace.min.js"></script>
<script src="${pageContent.request.contextPath}/blog/resources/templates/ace-master/read-more/read-more.js"></script>
<script src="${pageContent.request.contextPath}/blog/resources/templates/ace-master/assets/js/prettify.min.js"></script>

<!-- jQuery -->
<script src="${pageContent.request.contextPath}/blog/resources/templates/startbootstrap-blog-home-gh-pages/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="${pageContent.request.contextPath}/blog/resources/templates/startbootstrap-blog-home-gh-pages/js/bootstrap.min.js"></script>
<script type="text/javascript">
	jQuery(function($) {

		window.prettyPrint && prettyPrint();
		$('#id-check-horizontal').removeAttr('checked').on('click', function(){
			$('#dt-list-1').toggleClass('dl-horizontal').prev().html(this.checked ? '&lt;dl class="dl-horizontal"&gt;' : '&lt;dl&gt;');
		});

	})
</script>

</body>

</html>
