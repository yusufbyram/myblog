


<%@ page  contentType="text/html; charset=iso-8859-9"
	pageEncoding="iso-8859-9"%>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//TR" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="TR">
<head>
<c:set var="decorator" value="menu-decorator" scope="session" />


	<title>��erik</title>

<!-- Meta, title, CSS, favicons, etc. -->



</head>
<body>




	<form:form method="post" commandName="form" id="form"
		autocomplete="off" role="form">

		<div class="row">
			<div class="col-md-12">
				<div class="x_panel">


					<div class="col-md-2">
						<span>Kategori</span>
					</div>
					<div class="col-md-10">

						<div class="form-group">


							<form:select path="category.id" id="catId" name="category.id"
								tabindex="1" class="form-control">
								<form:option value="">SE��N�Z</form:option>
								<c:forEach items="${categoryList}" var="catList">

									<form:option value="${catList.id}">${catList.categoryName}</form:option>
								</c:forEach>

							</form:select>

						</div>
					</div>



					<div class="col-md-2">
						<span>Ba�l�k</span>
					</div>
					<div class="col-md-10 form-group">

						<form:input class="form-control" id="title" path="title"
							name="title" />
					</div>

					<div class="col-md-2 ">
						<span>��erik</span>
					</div>
					<div class="col-md-10 form-group">

						<form:input class="form-control" id="title" path="content"
							name="content" />

					</div>

					<div class="col-md-2 ">
						<span>Code ��erik</span>
					</div>

					<div class="col-md-10 form-group">
					<div class="wysiwyg-editor" id="editor1"    ></div>
					</div>



					<div class="col-md-2 ">
						<span>Tarih</span>
					</div>
					<div class="col-md-10 form-group  ">
						<form:input class="form-control" id="publishDate"
							path="publishDate" name="publishDate" />

					</div>

					<div class="col-md-2 ">
						<span>Yazan</span>
					</div>

					<div class="col-md-10 form-group">

						<form:input class="form-control" id="blogger" path="blogger"
							name="blogger" />

					</div>

					<div class="form-group col-md-12">
						<div class="pull-right">
							<button type="submit" id="save" class="btn btn-white btn-info btn-bold">
								<i class="ace-icon fa fa-floppy-o bigger-120 blue"></i> Save
							</button>

						</div>

					</div>
				</div>
			</div>
		</div>

		<form:hidden    path="codeContent"  id ="codeContent"  ></form:hidden>

	</form:form>

	<c:choose>
		<c:when test="${not empty contentList}">

                      Kategoriler
                                <table
				class="table table-striped table-bordered" border="1">
				<thead>
					<tr>
						<th>Ba�l�k</th>
						<th>��erik</th>
						<th>Tarih</th>
						<th>Yazan</th>
						<th>Sil</th>
						<th>D�zenle</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${contentList}">
						<tr>
							<td><c:out value="${item.title}" /></td>
							<td><c:out value="${item.content}" /></td>
							<td><c:out value="${item.publishDate}" /></td>
							<td><c:out value="${item.blogger}" /></td>
							<td><a
								href="${pageContext.request.contextPath}/content/delete/${item.id}"
								class="btn btn-primary blockUI"> <i class="fa fa-trash "></i></td>
							<td><a
								href="${pageContext.request.contextPath}/content/edit/${item.id}"
								class="btn btn-primary blockUI"> <i class="fa fa-edit "></i></td>
						</tr>
					</c:forEach>

					<c:url var="firstUrl" value="${pageContext.request.contextPath}/content/add?page=1" />
					<c:url var="lastUrl" value="${pageContext.request.contextPath}/content/add?page=${page.totalPages}" />
					<c:url var="prevUrl" value="${pageContext.request.contextPath}/content/add?page=${currentIndex - 1}" />
					<c:url var="nextUrl" value="${pageContext.request.contextPath}/content/add?page=${currentIndex + 1}" />





				</tbody>

			</table>
			<ul class="pagination">
				<c:choose>
					<c:when test="${currentIndex == 1}">
						<a href="#"><<</a>
						<a href="#"><</a>
					</c:when>
					<c:otherwise>
						<a href="${firstUrl}">�lk Kay�t</a>
						<a href="${prevUrl}">�nceki</a>
					</c:otherwise>
				</c:choose>

				<c:choose>
					<c:when test="${i == 1}">
						<c:set var="currentValue" scope="session" value="0" />
					</c:when>
					<c:otherwise>
						<c:set var="currentValue" scope="session" value="-1" />
					</c:otherwise>
				</c:choose>

				<c:forEach var="i" begin="${currentIndex-0}" end="${currentIndex+2}">

					<c:url var="pageUrl" value="${pageContext.request.contextPath}/content/add?page=${i}" />
					<c:choose>
						<c:when test="${i == currentIndex}">
							<a href="${pageUrl}"><font size="5" color="orange"> <c:if
										test="${i != 0}">${i}</c:if></font></a>

						</c:when>
						<c:otherwise>
							<c:choose>
								<c:when test="${   i <= page.totalPages    }">
									<a href="${pageUrl}"><c:if test="${i != 0}">${i}</c:if></a>
								</c:when>
							</c:choose>

						</c:otherwise>
					</c:choose>
				</c:forEach>
				<c:choose>
					<c:when test="${currentIndex == page.totalPages}">
						<a href="#">></a>
						<a href="#">>></a>
					</c:when>
					<c:otherwise>
						<a href="${nextUrl}">Sonraki</a>
						<a href="${lastUrl}">Son Kay�t</a>
					</c:otherwise>
				</c:choose>
			</ul>
		</c:when>

	</c:choose>




	<script src="${pageContent.request.contextPath}/blog/resources/templates/ace-master/assets/js/jquery-2.1.4.min.js"></script>

	<!-- <![endif]-->

	<!--[if IE]>
	<script src="${pageContent.request.contextPath}/blog/resources/templates/ace-master/assets/js/jquery-1.11.3.min.js"></script>
	<![endif]-->
	<script type="text/javascript">
		if('ontouchstart' in document.documentElement) document.write("<script src='${pageContent.request.contextPath}/blog/resources/templates/ace-master/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
	</script>
	<script src="${pageContent.request.contextPath}/blog/resources/templates/ace-master/assets/js/bootstrap.min.js"></script>

	<!-- page specific plugin scripts -->
	<script src="${pageContent.request.contextPath}/blog/resources/templates/ace-master/assets/js/jquery-ui.custom.min.js"></script>
	<script src="${pageContent.request.contextPath}/blog/resources/templates/ace-master/assets/js/jquery.ui.touch-punch.min.js"></script>
	<script src="${pageContent.request.contextPath}/blog/resources/templates/ace-master/assets/js/markdown.min.js"></script>
	<script src="${pageContent.request.contextPath}/blog/resources/templates/ace-master/assets/js/bootstrap-markdown.min.js"></script>
	<script src="${pageContent.request.contextPath}/blog/resources/templates/ace-master/assets/js/jquery.hotkeys.index.min.js"></script>
	<script src="${pageContent.request.contextPath}/blog/resources/templates/ace-master/assets/js/bootstrap-wysiwyg.min.js"></script>
	<script src="${pageContent.request.contextPath}/blog/resources/templates/ace-master/assets/js/bootbox.js"></script>

	<!-- ace scripts -->
	<script src="${pageContent.request.contextPath}/blog/resources/templates/ace-master/assets/js/ace-elements.min.js"></script>
	<script src="${pageContent.request.contextPath}/blog/resources/templates/ace-master/assets/js/ace.min.js"></script>


	<script type="text/javascript">

		$(document).ready(function () {
			$("#save").click(function () {


			 $("#codeContent").val($("#editor1").html());


			});
		});

					jQuery(function($){





			function showErrorAlert (reason, detail) {
				var msg='';
				if (reason==='unsupported-file-type') { msg = "Unsupported format " +detail; }
				else {
					//console.log("error uploading file", reason, detail);
				}
				$('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+
						'<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
			}

			//$('#editor1').ace_wysiwyg();//this will create the default editor will all buttons

			//but we want to change a few buttons colors for the third style

						$('#editor1').ace_wysiwyg({
							toolbar:
									[
										'font',
										null,
										'fontSize',
										null,
										{name:'bold', className:'btn-info'},
										{name:'italic', className:'btn-info'},
										{name:'strikethrough', className:'btn-info'},
										{name:'underline', className:'btn-info'},
										null,
										{name:'insertunorderedlist', className:'btn-success'},
										{name:'insertorderedlist', className:'btn-success'},
										{name:'outdent', className:'btn-purple'},
										{name:'indent', className:'btn-purple'},
										null,
										{name:'justifyleft', className:'btn-primary'},
										{name:'justifycenter', className:'btn-primary'},
										{name:'justifyright', className:'btn-primary'},
										{name:'justifyfull', className:'btn-inverse'},
										null,
										{name:'createLink', className:'btn-pink'},
										{name:'unlink', className:'btn-pink'},
										null,
										{name:'insertImage', className:'btn-success'},
										null,
										'foreColor',
										null,
										{name:'undo', className:'btn-grey'},
										{name:'redo', className:'btn-grey'}
									],
							'wysiwyg': {
								fileUploadError: showErrorAlert
							}
						}).prev().addClass('wysiwyg-style2');



			/**
			 //make the editor have all the available height
			 $(window).on('resize.editor', function() {
		var offset = $('#editor1').parent().offset();
		var winHeight =  $(this).height();

		$('#editor1').css({'height':winHeight - offset.top - 10, 'max-height': 'none'});
	}).triggerHandler('resize.editor');
			 */








			//RESIZE IMAGE

			//Add Image Resize Functionality to Chrome and Safari
			//webkit browsers don't have image resize functionality when content is editable
			//so let's add something using jQuery UI resizable
			//another option would be opening a dialog for user to enter dimensions.
			if ( typeof jQuery.ui !== 'undefined' && ace.vars['webkit'] ) {

				var lastResizableImg = null;
				function destroyResizable() {
					if(lastResizableImg == null) return;
					lastResizableImg.resizable( "destroy" );
					lastResizableImg.removeData('resizable');
					lastResizableImg = null;
				}

				var enableImageResize = function() {
					$('.wysiwyg-editor')
							.on('mousedown', function(e) {
								var target = $(e.target);
								if( e.target instanceof HTMLImageElement ) {
									if( !target.data('resizable') ) {
										target.resizable({
											aspectRatio: e.target.width / e.target.height,
										});
										target.data('resizable', true);

										if( lastResizableImg != null ) {
											//disable previous resizable image
											lastResizableImg.resizable( "destroy" );
											lastResizableImg.removeData('resizable');
										}
										lastResizableImg = target;
									}
								}
							})
							.on('click', function(e) {
								if( lastResizableImg != null && !(e.target instanceof HTMLImageElement) ) {
									destroyResizable();
								}
							})
							.on('keydown', function() {
								destroyResizable();
							});
				}

				enableImageResize();

				/**
				 //or we can load the jQuery UI dynamically only if needed
				 if (typeof jQuery.ui !== 'undefined') enableImageResize();
				 else {//load jQuery UI if not loaded
			//in Ace demo ./components will be replaced by correct components path
			$.getScript("assets/js/jquery-ui.custom.min.js", function(data, textStatus, jqxhr) {
				enableImageResize()
			});
		}
				 */
			}


		});
	</script>


</body>
</html>