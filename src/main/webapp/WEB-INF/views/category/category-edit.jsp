<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<c:set var="decorator" value="menu-decorator" scope="session" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">


</head>
<body>


	<form:form method="post" commandName="form" id="form"
		class="form-horizontal" autocomplete="off" role="form">

		<div class="x_panel">
		${error}
			<div class="col-md-2">
				<span>Kategori</span>
			</div>
			<div class="col-md-10 form-group">

				<form:input class="form-control" id="categoryName"
					path="categoryName" name="categoryName" />
			</div>

			<div class="col-md-2">
				<span>Kategori Aciklama</span>
			</div>
			<div class="col-md-10 form-group">

				<form:input class="form-control" id="categoryDescription"
					path="categoryDescription" name="categoryDescription" />

			</div>

			<div class="form-group col-md-12">
				<div class="pull-right">
					<button type="submit" class="btn btn-white btn-info btn-bold">
						<i class="ace-icon fa fa-floppy-o bigger-120 blue"></i> Save
					</button>

				</div>

			</div>

		</div>
	</form:form>

<c:choose>
                        <c:when test="${not empty categoryList}">
                      
                      Kategoriler
                                <table class="table table-striped table-bordered"  border="1" >
                                    <thead>
                                    <tr>
                                        <th>Kategori Adi</th>
                                        <th>Kategori Aciklama</th>
                                        <th>sil</th>
                                        <th>Düzenle</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="item" items="${categoryList}">
                                        <tr>
                                            <td><c:out value="${item.categoryName}"/></td>
                                            <td><c:out value="${item.categoryDescription}"/></td>
                                             <td> <a href="delete?id=${item.id}"
               class="btn btn-primary blockUI"/> <i class="fa fa-trash "/></td>
                                          <td> <a href="edit/${item.id}"
               class="btn btn-primary blockUI"/> <i class="fa fa-edit "/></td>
                                        </tr>
                                    </c:forEach>
                                    
                                    				<c:url var="firstUrl" value="${pageContext.request.contextPath}/category/add?page=1" />
				<c:url var="lastUrl" value="add?page=${page.totalPages}" />
				<c:url var="prevUrl" value="add?page=${currentIndex - 1}" />
				<c:url var="nextUrl" value="add?page=${currentIndex + 1}" />



				          
                                    
                                    </tbody>
                                    
                                </table>
                                <ul class="pagination">
					<c:choose>
						<c:when test="${currentIndex == 1}">
							<a href="#"><<</a>
							<a href="#"><</a>
						</c:when>
						<c:otherwise>
							<a href="${firstUrl}">İlk Kayıt</a>
							<a href="${prevUrl}">Önceki</a>
						</c:otherwise>
					</c:choose>

					<c:choose>
						<c:when test="${i == 1}">
							<c:set var="currentValue" scope="session" value="0" />
						</c:when>
						<c:otherwise>
							<c:set var="currentValue" scope="session" value="-1" />
						</c:otherwise>
					</c:choose>



					<c:forEach var="i" begin="${currentIndex-0}"
						end="${currentIndex+2}">






						<c:url var="pageUrl" value="add?page=${i}" />
						<c:choose>
							<c:when test="${i == currentIndex}">
								<a href="${pageUrl}"><font size="5" color="orange"> <c:if
											test="${i != 0}">${i}</c:if></font></a>

							</c:when>
							<c:otherwise>
								<c:choose>
									<c:when test="${   i <= page.totalPages    }">
										<a href="${pageUrl}"><c:if test="${i != 0}">${i}</c:if></a>
									</c:when>
								</c:choose>

							</c:otherwise>
						</c:choose>
					</c:forEach>
					<c:choose>
						<c:when test="${currentIndex == page.totalPages}">
							<a href="#">></a>
							<a href="#">>></a>
						</c:when>
						<c:otherwise>
							<a href="${nextUrl}">Sonraki</a>
							<a href="${lastUrl}">Son Kayıt</a>
						</c:otherwise>
					</c:choose>                               
                         </ul> 
                        </c:when>
                       
                    </c:choose>



</body>
</html>