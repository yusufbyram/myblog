<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="code"  uri="/WEB-INF/Custom-Tld/CodeView.tld"   %>

<head>

    <c:set var="decorator" value="anasayfa-decorator" scope="session" />

</head>

<body>



<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">




                <h1 class="page-header">
                    <h2>
                        <a href="#">${content.title}</a>
                    </h2>
                    <!--  <small>Secondary Text</small>-->
                    by <a href="index.php">${content.blogger}</a>
                    <p><span class="glyphicon glyphicon-time"></span> ${content.publishDate}</p>

                </h1>
                <span class="more">${content.content}</span>

                <c:choose>
                    <c:when test="${content.contentPhoto != null}">

                        <hr>
                        <img class="img-responsive" src="http://placehold.it/900x300" alt="">
                        <hr>

                    </c:when>
                    <c:otherwise>



                    </c:otherwise>
                </c:choose>



                <c:choose>

                    <c:when test="${content.codeContent != null}">

                        <div class="row">

                                ${content.codeContent}




                                        <%--   <code:codeView > ${content.codeContent}</code:codeView>--%>
                                             <%-- <code:codeView > ${item.codeContent}</code:codeView> --%>
                        </div>

                    </c:when>
                    <c:otherwise>



                    </c:otherwise>
                </c:choose>


                <!-- First Blog Post -->









            <p class="lead">

            </p>


            <hr>




            <!-- Pager -->


        </div>

        <!-- Blog Sidebar Widgets Column -->


    </div>
    <!-- /.row -->

    <hr>

    <!-- Footer -->


</div>
<!-- /.container -->





</body>
