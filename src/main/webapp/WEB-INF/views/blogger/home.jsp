<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="code"  uri="/WEB-INF/Custom-Tld/CodeView.tld"   %>


<head>



    <c:set var="decorator" value="anasayfa-decorator" scope="session" />


</head>

<body>

    <!-- Navigation -->


    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">



                <c:forEach var="item" items="${contentList}">

                <h1 class="page-header">
                    <h2>
                        <a href="#">${item.title}</a>
                    </h2>
                    <!--  <small>Secondary Text</small>-->
                    by <a href="index.php">${item.blogger}</a>
                    <p><span class="glyphicon glyphicon-time"></span> ${item.publishDate}</p>

                </h1>
<span>${item.content}</span>

                    <c:choose>
                        <c:when test="${item.contentPhoto != null}">

                            <hr>
                            <img class="img-responsive" src="http://placehold.it/900x300" alt="">
                            <hr>

                        </c:when>
                        <c:otherwise>



                        </c:otherwise>
                    </c:choose>



                    <c:choose>

                    <c:when test="${item.codeContent != null}">

                <div class="row">


                        <%-- <code:codeView > ${item.codeContent}</code:codeView> --%>
                </div>

                    </c:when>
                        <c:otherwise>



                        </c:otherwise>
                    </c:choose>


<!-- First Blog Post -->


                    <form method="get" action="index/${item.id}">

                        <input type="submit" value="devamını okuyunuz" />




                    </form>




                   
                </c:forEach>





 <p class="lead">

                </p>


                <hr>



	<c:url var="firstUrl" value="/home/category?page=1" />
				<c:url var="lastUrl" value="/blogger/index?page=${page.totalPages}" />
				<c:url var="prevUrl" value="/blogger/index?page=${currentIndex - 1}" />
				<c:url var="nextUrl" value="/blogger/index?page=${currentIndex + 1}" />



                <!-- Pager -->
                <ul class="pager">

				<c:choose>
					<c:when test="${currentIndex == 1}">

					</c:when>
					<c:otherwise>


						<li class="previous">
                        <a href="${prevUrl}">&larr; Önceki</a>
                    </li>

					</c:otherwise>
				</c:choose>

				<c:choose>
					<c:when test="${i == 1}">
						<c:set var="currentValue" scope="session" value="0" />
					</c:when>
					<c:otherwise>
						<c:set var="currentValue" scope="session" value="-1" />
					</c:otherwise>
				</c:choose>

				<c:forEach var="i" begin="${currentIndex-0}" end="${currentIndex+2}">

					<c:url var="pageUrl" value="/blogger/index?page=${i}" />
					<c:choose>
						<c:when test="${i == currentIndex}">
							<a href="${pageUrl}"><font size="5" color="orange"> <c:if
										test="${i != 0}">${i}</c:if></font></a>

						</c:when>
						<c:otherwise>
							<c:choose>
								<c:when test="${   i <= page.totalPages    }">
									<a href="${pageUrl}"><c:if test="${i != 0}">${i}</c:if></a>
								</c:when>
							</c:choose>

						</c:otherwise>
					</c:choose>
				</c:forEach>
				<c:choose>
					<c:when test="${currentIndex == page.totalPages}">

					</c:when>
					<c:otherwise>

					<li class="next">
                        <a href="${nextUrl}">Sonraki &rarr;</a>
                    </li>
					</c:otherwise>
				</c:choose>






                </ul>

            </div>

            <!-- Blog Sidebar Widgets Column -->


        </div>
        <!-- /.row -->

        <hr>


    </div>
    <!-- /.container -->





</body>

