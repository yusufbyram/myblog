<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

	<li ><a href="/blog/category/add"> <i
			class="menu-icon fa fa-pencil"></i> <span class="menu-text">
				Kategori Ekle </span>
	</a> <b class="arrow"></b></li>

	<li ><a href="/blog/content/add"> <i
			class="menu-icon fa fa-pencil-square-o"></i> <span class="menu-text">
				İçerik Ekle </span>
	</a> <b class="arrow"></b></li>