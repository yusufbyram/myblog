package com.yusuf.blog.blogcontent;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ContentService {

	@Autowired
	ContentRepository contentRepository;

	public Boolean save(Content content) {
		Boolean returnValue = false;

		try {
			contentRepository.save(content);
			returnValue = true;
		} catch (Exception e) {
			e.printStackTrace();

		}

		return returnValue;
	}

	public Page<Content> findAll(Pageable pageable) {
		return contentRepository.findAll(pageable);
	}

	public Content findOne(Long id) {
		Content contentBean = contentRepository.findOne(id);

		return contentBean;

	}

	public Boolean delete(Content content) {
		Boolean returnValue = false;
		try {
			contentRepository.delete(content);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return returnValue;
	}

	public Page<Content> findCategoryId(Long categoryId,Pageable pageable) {
		return contentRepository.findByCategoryId(categoryId,pageable);
	}

}
