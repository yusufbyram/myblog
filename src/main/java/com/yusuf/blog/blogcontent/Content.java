package com.yusuf.blog.blogcontent;

import com.yusuf.blog.category.Category;

import javax.persistence.*;

@Entity(name = "X_BLOG_CONTENT")
public class Content {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Category category;

    private String title;
    private String publishDate;
    private String content;
    private String blogger;
    private Byte[] contentPhoto;
    private String codeContent;

    public String getCodeContent() {
        return codeContent;
    }

    public void setCodeContent(String codeContent) {
        this.codeContent = codeContent;
    }


    public Byte[] getContentPhoto() {
        return contentPhoto;
    }

    public void setContentPhoto(Byte[] contentPhoto) {
        this.contentPhoto = contentPhoto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getBlogger() {
        return blogger;
    }

    public void setBlogger(String blogger) {
        this.blogger = blogger;
    }

}
