package com.yusuf.blog.blogcontent;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContentRepository extends
		PagingAndSortingRepository<Content, Long> {

	public Page<Content> findByCategoryId(Long categoryId,Pageable pageable);

}
