package com.yusuf.blog.blogcontent;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.yusuf.blog.category.CategoryService;
import com.yusuf.blog.service.SecurityService;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
@RequestMapping(headers = "Accept=text/html" , value=  "content")
public class ContentController {

	private static final String BASE_PATH = "/content/content-";

	private static final String PAGE_DEFAULT = "redirect:/content/search";

	private static final String PAGE_EDIT = BASE_PATH + "edit";

	private static final String PAGE_SEARCH = BASE_PATH + "search";

	private static final String PAGE_VIEW = BASE_PATH + "view";
	
	@Autowired 
	ContentService contentService;
	
	@Autowired
	CategoryService categoryService;
	
	@Autowired
	SecurityService securityService;
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String getCategoryAdd(Model model, Pageable pageable,
			Content content) {

		Sort sort = new Sort(Sort.Direction.ASC, "name");


		Pageable pagee = new PageRequest(pageable.getPageNumber(),
				pageable.getPageSize());

		Page<Content> page = contentService.findAll(pagee);

		model.addAttribute("userName", securityService.getPrincipal());
		model.addAttribute("form", content);
		model.addAttribute("ContentList", page.getContent());

		model.addAttribute("categoryList", categoryService.findAll()  );

		model.addAttribute("page", page);
		model.addAttribute("beginIndex", 1);
		model.addAttribute("endIndex", page.getTotalPages());

		model.addAttribute("currentIndex", pagee.getPageNumber() + 1);

		return "/content/content-edit";
	}
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String postCategory(Model model,
							   @Valid @ModelAttribute("form") Content content,
							   BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			model.addAttribute("hata", "bu alan bos gecilemez");
			return "redirect:/content/add";
		}

		contentService.save(content);

		return "redirect:/content/add";
	}
	
}
