package com.yusuf.blog.blogcontent;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ContentJsonResponse {

	@JsonProperty("content")
	private List<Content> content;

	public List<Content> getContent() {
		return content;
	}

	public void setContent(List<Content> content) {
		this.content = content;
	}

	

}
