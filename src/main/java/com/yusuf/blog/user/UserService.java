package com.yusuf.blog.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

	@Autowired
	UserRepository userRepo;

	public User getUser(String userName) {

		User userBean = userRepo.findByUserName(userName);

		return userBean;

	}

}
