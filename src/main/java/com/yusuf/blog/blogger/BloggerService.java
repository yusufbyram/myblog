package com.yusuf.blog.blogger;

import com.yusuf.blog.blogcontent.Content;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by yusuf on 30.07.2017.
 */

@Service
public class BloggerService {

    @Autowired
    BloggerRepository repository;

    public List<Content> getFindAll() {
        return repository.findAll();

    }


}
