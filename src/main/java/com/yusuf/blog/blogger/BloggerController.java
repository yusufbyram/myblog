package com.yusuf.blog.blogger;

import com.yusuf.blog.blogcontent.Content;
import com.yusuf.blog.blogcontent.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by yusuf on 14.05.2017.
 */

@Controller
@RequestMapping("/blogger")
public class BloggerController {

    @Autowired
    BloggerService bloggerService;

    @Autowired
    ContentService contentService;

    @RequestMapping(value = "/index",method = RequestMethod.GET)
    public String getBlogger(Model model, Pageable pageable){


        Pageable pagee = new PageRequest(pageable.getPageNumber(),
                pageable.getPageSize());

        Page<Content> page = contentService.findAll(pagee);

      model.addAttribute("contentList",page.getContent());


        model.addAttribute("page", page);
        model.addAttribute("beginIndex", 1);
        model.addAttribute("endIndex", page.getTotalPages());

        model.addAttribute("currentIndex", pagee.getPageNumber() + 1);


        return "/blogger/home";
    }
@RequestMapping(value = "index/{id}" , method = RequestMethod.GET)
    public String getContentId(@PathVariable("id") Long id,Model model ){

    Content content = contentService.findOne(id);
    model.addAttribute("content",content);
    return "/blogger/homeDetail";

}

}
