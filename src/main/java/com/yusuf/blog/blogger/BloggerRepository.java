package com.yusuf.blog.blogger;

import com.yusuf.blog.blogcontent.Content;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by yusuf on 30.07.2017.
 */


@Repository
public interface BloggerRepository  extends   JpaRepository<Content,Long>{

}
