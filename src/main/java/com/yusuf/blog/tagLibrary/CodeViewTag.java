package com.yusuf.blog.tagLibrary;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.io.StringWriter;

/**
 * Created by yusuf on 06.08.2017.
 */
public class CodeViewTag extends SimpleTagSupport {
    StringWriter sw = new StringWriter();
    String startTag="";
    String endTag="";

    public void doTag() throws  JspException,IOException {

startTag="<div class=\"col-xs-12\">\n" +
        "\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-box\">\n" +
        "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<h4 class=\"smaller\">\n" +
        "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<i class=\"ace-icon fa fa-code\"></i>\n" +
        "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</h4>\n" +
        "\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
        "\n" +
        "\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-body\">\n" +
        "\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"widget-main\">\n" +
        "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<pre class=\"prettyprint linenums\">";


        endTag=" </pre>\n" +
                "\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
                "\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
                "\t\t\t\t\t\t\t\t\t\t\t</div>";




        getJspBody().invoke(sw);
        getJspContext().getOut().println( startTag+ sw.toString()+endTag);


    }






}
