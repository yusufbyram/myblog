package com.yusuf.blog.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableJpaRepositories(basePackages="com.yusuf.blog" )
@EnableTransactionManagement
public class SpringJpaConfiguration {

	@Bean
	  public EntityManagerFactory entityManagerFactory() {

	    HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
	    vendorAdapter.setGenerateDdl(true);

	    Properties propertiesJpa =   new Properties();
	    propertiesJpa.put("hibernate.hbm2ddl.auto", "update");
	    
	    
	    LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
	    factory.setJpaVendorAdapter(vendorAdapter);
	   
	    factory.setPackagesToScan("com.yusuf.blog");
	    factory.setJpaProperties( propertiesJpa);
	    factory.setDataSource(dataSource());
	    factory.afterPropertiesSet();

	    return factory.getObject();
	  }

	  @Bean
	  public PlatformTransactionManager transactionManager() {

	    JpaTransactionManager txManager = new JpaTransactionManager();
	    txManager.setEntityManagerFactory(entityManagerFactory());
	    return txManager;
	  }
	
	
	   
  	 @Bean
	   public DataSource dataSource(){
	      DriverManagerDataSource dataSource = new DriverManagerDataSource();
	  /*    dataSource.setDriverClassName("com.mysql.jdbc.Driver");
	      dataSource.setUrl("jdbc:mysql://localhost:3306/blogproje");
	      dataSource.setUsername( "root" );
	      dataSource.setPassword( "MySql2017" );
*/

	  dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		 dataSource.setUrl("jdbc:mysql://172.17.0.1:6666/blogproje");
		 dataSource.setUsername( "root" );
		 dataSource.setPassword( "yusuf2017" );


		/* dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		 dataSource.setUrl("jdbc:mysql://94.73.150.32/blog");
		 dataSource.setUsername( "blog" );
		 dataSource.setPassword( "EDhp78K6" );
*/


			/*	 dataSource.setDriverClassName("org.postgresql.Driver");
		 dataSource.setUrl("babar.elephantsql.com");
		 dataSource.setUsername( "senypyhl" );
		 dataSource.setPassword( "H6lXEVUw5Iq29V3LnH6DI59JCiwbmYjq" );
*/

	      return dataSource;
	   
  	 }

}