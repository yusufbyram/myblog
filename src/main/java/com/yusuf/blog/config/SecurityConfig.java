package com.yusuf.blog.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.yusuf.blog.auth.AuthenticationService;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	DataSource dataSource;

	@Autowired
	AuthenticationService authService;

	@Autowired
	public void configureGlobalSecurity(AuthenticationManagerBuilder auth)
			throws Exception {

		
		
		Md5PasswordEncoder encode = new Md5PasswordEncoder();
		
		//auth.userDetailsService(authService);
		ShaPasswordEncoder encoder = new ShaPasswordEncoder();
		auth.userDetailsService(authService).passwordEncoder(encode);
		


		System.out.println("UserDetailsService configuration successful.....");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.authorizeRequests().antMatchers("/", "/home/**","/blogger/**").permitAll()
				.antMatchers("/category/**","/content/**").access("hasRole('ROLE_ADMIN')")
				.antMatchers("/db/**")
				.access("hasRole('ADMIN') and hasRole('DBA')")
				// .and().rememberMe().tokenValiditySeconds(15)
				.and().formLogin().loginPage("/loginPage").permitAll().and()
				.exceptionHandling().accessDeniedPage("/accessdenied");

		/*
		 * configure edilecek http.rememberMe(). key("rem-me-key").
		 * rememberMeParameter("remember-me").
		 * rememberMeCookieName("my-remember-me"). tokenValiditySeconds(15);
		 */

	}

}