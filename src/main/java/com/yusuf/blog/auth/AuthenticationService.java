package com.yusuf.blog.auth;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.yusuf.blog.user.User;
import com.yusuf.blog.user.UserService;

@Service
public class AuthenticationService implements UserDetailsService {

	@Autowired
	UserService userService;

	
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {

		User userBean = userService.getUser(username);
		GrantedAuthority authority = new SimpleGrantedAuthority(
				userBean.getRole());
		UserDetails userDetails = (UserDetails) new org.springframework.security.core.userdetails.User(
				userBean.getUserName(), userBean.getPassWord(),
				Arrays.asList(authority));

		return userDetails;
	}
}
