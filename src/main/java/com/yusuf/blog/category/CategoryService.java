package com.yusuf.blog.category;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.yusuf.blog.category.Category;
import com.yusuf.blog.category.CategoryRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolationException;

@Service
@Transactional
public class CategoryService {

	@Autowired
	CategoryRepository categoryRepository;

	public Boolean delete(Category category)   {
		Boolean returnValue = false;

			categoryRepository.delete(category);

		return returnValue;
	}

	public Category findOne(Long id) {
		Category categoryBean = categoryRepository.findOne(id);

		return categoryBean;

	}

	public Boolean save(Category category) {
		Boolean returnValue = false;

		try {
			categoryRepository.save(category);
			returnValue = true;
		} catch (Exception e) {
			e.printStackTrace();

		}

		return returnValue;
	}

	/*
	 * public List<Category> findAll() {
	 * 
	 * List<Category> categoryList = categoryRepository.findAll();
	 * 
	 * return categoryList; }
	 */

	public Page<Category> findAll(Pageable pageable) {
		return categoryRepository.findAll(pageable);
	}

	public List<Category> findAll() {
		return categoryRepository.findAll();
	}

}