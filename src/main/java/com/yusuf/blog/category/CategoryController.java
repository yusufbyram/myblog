package com.yusuf.blog.category;

import java.sql.SQLException;
import java.util.List;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.yusuf.blog.category.Category;
import com.yusuf.blog.category.CategoryService;
import com.yusuf.blog.service.SecurityService;

@Controller
@RequestMapping("/category")
public class CategoryController {

	private static final String BASE_PATH = "/category/category-";

	private static final String PAGE_DEFAULT = "redirect:/category/search";

	private static final String PAGE_EDIT = BASE_PATH + "edit";

	private static final String PAGE_SEARCH = BASE_PATH + "search";

	private static final String PAGE_VIEW = BASE_PATH + "view";

	@Autowired
	CategoryService categoryService;

	@Autowired
	SecurityService securityService;

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String getCategoryAdd(Model model, Pageable pageable,
			Category category) {

		Sort sort = new Sort(Sort.Direction.ASC, "name");


		Pageable pagee = new PageRequest(pageable.getPageNumber(),
				pageable.getPageSize());

		Page<Category> page = categoryService.findAll(pagee);

		model.addAttribute("userName", securityService.getPrincipal());
		model.addAttribute("form", category);
		model.addAttribute("categoryList", page.getContent());
		model.addAttribute("page", page);

		model.addAttribute("beginIndex", 1);
		model.addAttribute("endIndex", page.getTotalPages());

		model.addAttribute("currentIndex", pagee.getPageNumber() + 1);

		return "/category/category-edit";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String postCategory(Model model,
			@Valid @ModelAttribute("form") Category category,
			BindingResult bindingResult) {

		if (bindingResult.hasErrors()) {
			model.addAttribute("hata", "bu alan bos gecilemez");
			return "redirect:/category/add";
		}

		categoryService.save(category);

		return "redirect:add";
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String editCategoryGet(@PathVariable Long id, Pageable pageable,
			Model model) {
		Pageable pagee = new PageRequest(pageable.getPageNumber(),
				pageable.getPageSize());

		Category category = categoryService.findOne(id);

		Page<Category> page = categoryService.findAll(pagee);

		model.addAttribute("userName", securityService.getPrincipal());
		model.addAttribute("form", category);
		model.addAttribute("categoryList", page.getContent());
		model.addAttribute("page", page);
		model.addAttribute("beginIndex", 1);
		model.addAttribute("endIndex", page.getTotalPages());
		model.addAttribute("currentIndex", pagee.getPageNumber() + 1);

		return "/category/category-edit";
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public String editCategoryPost(@PathVariable Long id,@ModelAttribute("form") Category category, Pageable pageable,
			Model model) {
		Pageable pagee = new PageRequest(pageable.getPageNumber(),
				pageable.getPageSize());

		categoryService.save(category);
		Page<Category> page = categoryService.findAll(pagee);



		return "redirect:/blog/category/add";
	}

	
	
	@RequestMapping(value = "delete", method = RequestMethod.GET)
	public String deleteCategory(@RequestParam Long id, Pageable pageable,
			Model model)  {




		Category category = categoryService.findOne(id);

		try {
			categoryService.delete(category);
		}
catch(Exception ex){
			model.addAttribute("error","kayıt silinemedi");
}


		return "redirect:add";
	}

	@RequestMapping(value = "search", method = RequestMethod.GET)
	public String getSearch(Model model) {

		/*
		 * List<Category> categoryList = categoryService.findAll();
		 * model.addAttribute("categoryList", categoryList);
		 */
		return PAGE_SEARCH;

	}

}
