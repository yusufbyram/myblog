package com.yusuf.blog.category;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.yusuf.blog.category.Category;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface CategoryRepository extends
		PagingAndSortingRepository<Category, Long> {

	public List<Category> findAll();
	
	
	
}
