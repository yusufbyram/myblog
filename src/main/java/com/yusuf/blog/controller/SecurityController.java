package com.yusuf.blog.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.yusuf.blog.service.SecurityService;

@Controller
@RequestMapping("security")
public class SecurityController {

	@Autowired
	SecurityService securityService;

	@RequestMapping(value = "/accessdenied", method = RequestMethod.GET)
	public String accessDeniedPage(Model model) {
		model.addAttribute("user", securityService.getPrincipal());
		return "config/accessdenied";
	}
	 @RequestMapping(value = "login", method = RequestMethod.GET)
	    public String getLoginForm(Model m) {
	        return "login/login";
	    }
	 
	 
	 @RequestMapping(value = "cikis", method = RequestMethod.GET)
	    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
	        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	        if (auth != null) {
	            new SecurityContextLogoutHandler().logout(request, response, auth);
	        }
	         return "redirect:/blog/category/add";
	        		 
	 
	 }
	 

	  /*  @RequestMapping(value = "sorgu", method = RequestMethod.POST)
	    public String getSorguSonuc(Model m,
	            HttpSession httpSession, HttpServletRequest request) {
	        String breadClumb = "";
	        // Captcha kodunu kontrol et
	        final Captcha captcha = (Captcha) httpSession.getAttribute(Captcha.NAME);

	       
	            // Captcha kodunu kullanildiktan sonra kaldir
	            httpSession.removeAttribute(Captcha.NAME);

	            List<Sicil> siciller = sicilService.sicilBul(searchTerm);

	            sicilService.setFetchedSiciller(siciller);

	         
	            m.addAttribute("siciller", siciller);
	        
	        

	        return "sicil/sorgu";
	    }*/
	 
}
