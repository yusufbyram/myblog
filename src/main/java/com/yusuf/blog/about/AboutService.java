package com.yusuf.blog.about;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by yusuf on 14.08.2017.
 */
@Service
public class AboutService {

@Autowired
    AboutRepository aboutRepository;

    public void saveAbout (About about){
        aboutRepository.save(about);
    }

    public List<About> findAll (){
        return aboutRepository.findAll();
    }
    public About findOne(Long id){
        return aboutRepository.findOne(id);

    }
    public About findByTitle(String title){
        return aboutRepository.findByTitle(title);
    }

}
