package com.yusuf.blog.about;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by yusuf on 14.08.2017.
 */

@Controller
@RequestMapping("about")
public class AboutController {

    @Autowired
    AboutService aboutService;

    @RequestMapping( value ="myAbout" ,method = RequestMethod.GET)
public String getAbout(Model model){

About about = aboutService.findByTitle("Hakkimda");

model.addAttribute("about",about  );

        return "/about/about";

}



}
