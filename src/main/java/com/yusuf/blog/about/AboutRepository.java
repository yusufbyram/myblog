package com.yusuf.blog.about;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by yusuf on 14.08.2017.
 */
@Repository
interface AboutRepository extends JpaRepository<About,Long> {

 About findByTitle(String title);


}
