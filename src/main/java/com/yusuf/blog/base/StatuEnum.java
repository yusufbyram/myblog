package com.yusuf.blog.base;

public enum StatuEnum {

	ACTIVE("Aktive"), PASSIVE("Passive");

	private String displayName;

	StatuEnum(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return displayName;
	}

}
